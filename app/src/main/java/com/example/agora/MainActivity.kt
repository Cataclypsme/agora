package com.example.agora

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
// UI Packages and classes
import android.util.Log
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import android.widget.FrameLayout
import android.view.ViewGroup
//Video call logic packages
import io.agora.rtc.Constants
import io.agora.rtc.IRtcEngineEventHandler
import io.agora.rtc.RtcEngine
import io.agora.rtc.video.VideoCanvas
import io.agora.rtc.video.VideoEncoderConfiguration

class MainActivity : AppCompatActivity() {
    //Create the variables you use to create or join a call:
    private var mRtcEngine: RtcEngine? = null
    private val mRtcEventHandler = object : IRtcEngineEventHandler() {
        //Handle event when a remote user joins the channel.
        override fun onUserJoined(uid: Int, elapsed: Int) {
            runOnUiThread { setupRemoteVideo(uid) }
        }
    }
    companion object {
        private val LOG_TAG = MainActivity::class.java.simpleName

        private const val APP_ID = "353cb55fce044987adbeb6309619a9be"
        private const val CHANNEL = "Build"
        private const val TOKEN = "006353cb55fce044987adbeb6309619a9beIACmSNaVlDWvXSJFl0s33F/0schAICYAlWUpRFzVLU9lkN/dYXwAAAAAEACSu+6Caw1wYQEAAQBrDXBh"

        private const val PERMISSION_CODE = 22
    }

    //4.Updated onCreate
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (permissionsGranted()) {
            initializeApplication()
        } else {
            requestPermissions()
        }
    } // End of the onCreate

    // App Method

    // permissionsGranted
    private fun permissionsGranted(): Boolean {
        return ContextCompat.checkSelfPermission(this, android.Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
    }

    // requestPermissions
    private fun requestPermissions() {
        ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.CAMERA), PERMISSION_CODE)
    }

    //onRequestPermissionsResult
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
                    initializeApplication()
                } else {
                    finish()
                }
            }
        }
    }

    //initializeApplication
    // tag::initlalize_rtc[]
    private fun initializeApplication() {
        Log.i(LOG_TAG, "initializeApplication")
        // In the RtcEngine. This object calls all Agora methods
        try {
            mRtcEngine = RtcEngine.create(baseContext, APP_ID, mRtcEventHandler)
        } catch (e: Exception) {

        }

        // By Default, Video is disabled. Set it to enabled.
        mRtcEngine!!.enableVideo()

        // Set some standard configuration such as video resolution, FPS and bitrate.
        mRtcEngine!!.setVideoEncoderConfiguration(VideoEncoderConfiguration(VideoEncoderConfiguration.VD_640x360,
            VideoEncoderConfiguration.FRAME_RATE.FRAME_RATE_FPS_15,
            VideoEncoderConfiguration.STANDARD_BITRATE,
            VideoEncoderConfiguration.ORIENTATION_MODE.ORIENTATION_MODE_FIXED_PORTRAIT))

        // All videos are rendered on a SurfaceView. To create the interface:
        // 1. Fetch a reference to the FrameLayout defined in activity_main.xml
        // 2. Use Agora to create a SurfaceView object and add it as a child to the FrameLayout
        // 3. Pass the SurfaceView to Agora so that it renders our local video on it
        val localContainer = findViewById<FrameLayout>(R.id.local_container)
        val localFrame = RtcEngine.CreateRendererView(baseContext)
        localContainer.addView(localFrame, FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        mRtcEngine!!.setupLocalVideo(VideoCanvas(localFrame, VideoCanvas.RENDER_MODE_HIDDEN, 0))

        // Join the Agora Channel with TOKEN you defined earlier.
        // If TOKEN is empty, pass null.
        mRtcEngine!!.joinChannel(if (TOKEN.isEmpty()) null else TOKEN, CHANNEL, "", 0)

        // This method allows us to fallback to low resolution and low bitrate
        mRtcEngine!!.enableDualStreamMode(true)
    }
    // end::initlalize_rtc[]
    //setupRemoteVideo
    private fun setupRemoteVideo(uid: Int) {

        // Create the view to render remote video on
        val container = findViewById(R.id.remote_container) as FrameLayout
        //Only one remote view can join this channel
        if (container.childCount >= 1) {
            return
        }

        // Create the video renderer view. Agora SDK renders the view provided by the app.
        val surfaceView = RtcEngine.CreateRendererView(baseContext)
        container.addView(surfaceView)
        // Initializes the video view of a remote user.
        mRtcEngine!!.setupRemoteVideo(VideoCanvas(surfaceView, VideoCanvas.RENDER_MODE_FIT, uid))
        surfaceView.tag = uid
    }

    // onDestroy
    override fun onDestroy() {
        super.onDestroy()
        // Clean up all the resources used by Agora.
        mRtcEngine?.leaveChannel()
        RtcEngine.destroy()
        mRtcEngine = null
    }
}